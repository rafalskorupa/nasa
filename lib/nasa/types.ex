defmodule Nasa.Types do
  @moduledoc false

  @type gravity :: number()
  @type mass :: number()
  @type fuel :: number()
  @type stage_type :: :launch | :landing
end
