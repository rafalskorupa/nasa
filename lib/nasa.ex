defmodule Nasa do
  @moduledoc """
  Documentation for `Nasa`.
  """

  alias Nasa.Types

  @doc """
  Hello world.

  ## Examples

      iex> Nasa.hello()
      :space

  """
  def hello do
    :space
  end

  @doc """
  This is the entry point of application as described in document.

  It takes 2 arguments. F
  First one is the flight ship mass, and second is an array of 2 element tuples, 
  with the first element being land or launch directive, 
  and second element is the target planet gravity.
  """
  @spec call(Types.mass(), list({Types.stage_type(), Types.gravity()})) :: {:ok, Types.fuel()} | {:error, any()}
  def call(ship_mass, flight_route) do
    with {:ok, program} <- Nasa.Program.build(ship_mass, flight_route) do
      {:ok, Nasa.Fuel.calculate_fuel_required(program)}
    end
  end
end
