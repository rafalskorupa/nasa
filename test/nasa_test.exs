defmodule NasaTest do
  use ExUnit.Case
  doctest Nasa

  test "greets the space!" do
    assert Nasa.hello() == :space
  end

  alias Nasa.Solar.{Earth, Moon, Mars, Venus}

  describe "calculate_fuel_required" do
    test "Apollo 11 program" do
      assert {:ok, 51_898.0} ==
               Nasa.call(28_801, [
                 Earth.launch(),
                 Moon.landing(),
                 Moon.launch(),
                 Earth.landing()
               ])
    end

    test "Mission on Mars" do
      assert {:ok, 33_388} ==
               Nasa.call(14_606, [
                 {:launch, 9.807},
                 {:land, 3.711},
                 {:launch, 3.711},
                 {:land, 9.807}
               ])
    end

    test "Passenger ship" do
      assert {:ok, 212_161.0} =
               Nasa.call(75_432, [
                 {:launch, 9.807},
                 {:land, 1.62},
                 {:launch, 1.62},
                 {:land, 3.711},
                 {:launch, 3.711},
                 {:land, 9.807}
               ])
    end

    test "From Venus to Mars" do
      assert {:ok, 100_459.0} =
               Nasa.call(50_000, [
                 Venus.launch(),
                 Mars.landing(),
                 Mars.launch(),
                 Venus.landing()
               ])
    end

    test "staying in the base is free" do
      assert {:ok, 0} = Nasa.call(100_000, [])
    end

    test "when ship mass is invalid" do
      assert {:error, {:mass, "must be positive"}} = Nasa.call(-2, [])
    end

    test "when flight route is invalid" do
      assert {:error, {:route, "must be a list"}} =
               Nasa.call(75_432, {
                 {:launch, 9.807},
                 {:land, 1.62}
               })

      assert {:error, {:route, "[manouver: 9.807, land: -2] are invalid actions"}} =
               Nasa.call(75_432, [
                 {:launch, 9.807},
                 {:manouver, 9.807},
                 {:land, -2}
               ])
    end
  end
end
